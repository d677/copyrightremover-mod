# Copyright Remover mod

This mod removes the copyright footer on the main title screen.

Download from [CurseForge](https://www.curseforge.com/minecraft/mc-mods/copyright-remover).

Before:
![titlescreen without mod](repo-assets/minecraft_without_mod.png)

After:
![titlescreen with mod](repo-assets/minecraft_with_mod.png)

This also disables the credits showing up when clicking the footer.

# About mod
This is a one shot mod. So don't expect any updates.
If you feel something is wrong... Fork it!

Also it is much harder to remove features using fabric than adding them.
So I had to find out clever ways to make this mod work.
It would have been nice if they added a way to just cut out chunks of code.

## License

GNU LESSER GENERAL PUBLIC LICENSE v3.0 OR LATER (LGPLv3.0-or-later)