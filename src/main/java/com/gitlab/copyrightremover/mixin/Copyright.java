package com.gitlab.copyrightremover.mixin;

import net.minecraft.client.gui.screen.TitleScreen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArgs;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.invoke.arg.Args;

// Everything is changed in TitleScreen class
@Mixin(TitleScreen.class)
public class Copyright {
    // Prevents drawing of copyright text
    @ModifyArgs(method = "render(Lnet/minecraft/client/util/math/MatrixStack;IIF)V", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/gui/screen/TitleScreen;drawStringWithShadow(Lnet/minecraft/client/util/math/MatrixStack;Lnet/minecraft/client/font/TextRenderer;Ljava/lang/String;III)V", ordinal = 1))
    private void clearLine(Args args) {
        args.set(2, "");
    }

    // Prevents drawing of underline
    @ModifyArgs(method = "render(Lnet/minecraft/client/util/math/MatrixStack;IIF)V", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/gui/screen/TitleScreen;fill(Lnet/minecraft/client/util/math/MatrixStack;IIIII)V"))
    private void injected(Args args) {
        args.set(1, 0);
        args.set(2, 0);
        args.set(3, 0);
        args.set(4, 0);
    }

    // Prevents opening of credits page since it would be weird if it opens when I click nothing
    @Shadow
    private int copyrightTextWidth;

    @Inject(method = "mouseClicked(DDI)Z", at = @At("HEAD"))
    private void injected(CallbackInfoReturnable ci) {
        this.copyrightTextWidth = 0;
    }
}